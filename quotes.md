# Quotes

>If you want to achieve greatness stop asking for permission.
> -- Anonymous

>To live a creative life, we must lose our fear of being wrong. --Anonymous
